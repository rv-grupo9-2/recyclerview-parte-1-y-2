package com.example.reclyclerviewymodeladodedatos;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import Modelo.AlumnosDb;

public class AlumnoAlta extends AppCompatActivity {

    private static final int REQUEST_CODE_PERMISSION = 1;
    private Button btnGuardar, btnRegresar, btnEliminar;
    private Alumno alumno;
    private EditText txtNombre, txtMatricula, txtGrado;
    private ImageView imgAlumno;
    private TextView lblImagen;
    private String pathString;
    private int posicion;

    private AlumnosDb alumnosDb;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_alumno_alta);
        btnGuardar = (Button) findViewById(R.id.btnSalir);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);
        btnEliminar = (Button) findViewById(R.id.btnEliminar);
        txtMatricula = (EditText) findViewById(R.id.txtMatricula);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtGrado = (EditText) findViewById(R.id.txtGrado);
        imgAlumno = (ImageView) findViewById(R.id.imgAlumno);
        lblImagen = (TextView) findViewById(R.id.lblFoto);

        Bundle bundle = getIntent().getExtras();
        alumno = (Alumno) bundle.getSerializable("alumno");
        posicion = bundle.getInt("posicion", posicion);

        if(posicion >= 0 && alumno!=null){
            txtMatricula.setText(alumno.getMatricula());
            txtNombre.setText(alumno.getNombre());
            txtGrado.setText(alumno.getCarrera());

            imgAlumno.setImageURI(Uri.parse(alumno.getImg()));


            lblImagen.setText(alumno.getImg());
        }

        // Evento clic de la imagen
        imgAlumno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarImagen();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(posicion >= 0 && alumno!=null){
                    // Modificar Alumno
                    alumno.setMatricula(txtMatricula.getText().toString());
                    alumno.setNombre(txtNombre.getText().toString());
                    alumno.setCarrera(txtGrado.getText().toString());

                    // Cargar la imagen si es diferente a nulo
                    if (pathString != null) {
                        alumno.setImg(pathString);
                        lblImagen.setText(pathString);
                    }

                    ((Aplicacion) getApplication()).getAlumnos().get(posicion).setMatricula(alumno.getMatricula());
                    ((Aplicacion) getApplication()).getAlumnos().get(posicion).setNombre(alumno.getNombre());
                    ((Aplicacion) getApplication()).getAlumnos().get(posicion).setCarrera(alumno.getCarrera());
                    ((Aplicacion) getApplication()).getAlumnos().get(posicion).setImg(alumno.getImg());

                    alumnosDb = new AlumnosDb(getApplicationContext());
                    alumnosDb.updateAlumno(alumno);
                    Toast.makeText(getApplicationContext(), "Se modificó con éxito ", Toast.LENGTH_SHORT).show();
                } else {
                    // Agregar un nuevo alumno
                    String matricula = txtMatricula.getText().toString();
                    String nombre = txtNombre.getText().toString();
                    String grado = txtGrado.getText().toString();
                    String imagen = pathString;

                    if (matricula.isEmpty() || nombre.isEmpty() || grado.isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Faltó capturar datos", Toast.LENGTH_SHORT).show();
                        txtMatricula.requestFocus();
                    } else {
                        alumno = new Alumno();
                        alumno.setMatricula(matricula);
                        alumno.setNombre(nombre);
                        alumno.setCarrera(grado);
                        alumno.setImg(imagen);

                        alumnosDb = new AlumnosDb(getApplicationContext());

                        // Agregar el alumno nuevo a la bd y obtener el ID que se le asigno
                        long idNuevo = alumnosDb.inserAlumno(alumno);

                        if (idNuevo != -1) {
                            // Asignar el ID al alumno nuevo
                            alumno.setId((int) idNuevo);

                            // Cargar la imagen si es diferente a nulo
                            if (pathString != null) {
                                alumno.setImg(pathString);
                                lblImagen.setText(pathString);
                            }

                            ((Aplicacion) getApplication()).getAlumnos().add(alumno);

                            // Notificar al adaptador del cambio en los datos
                            ((Aplicacion) getApplication()).getAdaptador().notifyDataSetChanged();

                            setResult(Activity.RESULT_OK);
                            finish();

                            Toast.makeText(getApplicationContext(), "Se guardó con éxito", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Error al guardar alumno", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });

        btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(posicion >= 0 && alumno!=null){

                    AlertDialog.Builder confirmar = new AlertDialog.Builder(AlumnoAlta.this);
                    confirmar.setTitle("Eliminar Alumnos");
                    confirmar.setMessage("¿Desea eliminar al alumno?");
                    confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            AlumnosDb alumnosDb = new AlumnosDb(getApplicationContext());
                            alumnosDb.deleteAlumnos(alumno.getId());

                            ((Aplicacion) getApplication()).getAlumnos().remove(posicion);
                            ((Aplicacion) getApplication()).getAdaptador().notifyDataSetChanged();

                            setResult(Activity.RESULT_OK);
                            finish();

                            Toast.makeText(getApplicationContext(), "Se eliminó con éxito", Toast.LENGTH_SHORT).show();
                        }
                    });
                    confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //No hace nada
                        }
                    });
                    confirmar.show();

                }else{
                    Toast.makeText(getApplicationContext(), "No se puede eliminar el alumno", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void cargarImagen() {
        // Verificar si se tienen los permisos para leer el almacenamiento externo
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Solicitar permisos en tiempo de ejecución
            ActivityCompat.requestPermissions(this,new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE_PERMISSION);
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/");
            startActivityForResult(intent.createChooser(intent,"Seleccione la Aplicación"),REQUEST_CODE_PERMISSION);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Uri path = data.getData();
            pathString = path.toString();

            imgAlumno.setImageURI(path);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSION) {
            // Verificar si se otorgó el permiso
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permiso otorgado, se puede acceder al almacenamiento externo
                cargarImagen();
            } else {
                // Permiso denegado, no se puede acceder al almacenamiento externo.
                Toast.makeText(this, "Permiso denegado", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
