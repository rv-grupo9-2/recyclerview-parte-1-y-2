package com.example.reclyclerviewymodeladodedatos;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;

    private FloatingActionButton fbtnAgregar;
    private FloatingActionButton fbtnSalir;
    private Aplicacion app;

    private SearchView searchView;
    private MiAdaptador miAdaptador;

    private Alumno alumno;
    private int posicion=-1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Configuración de ToolBar como ActionBar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        Aplicacion app = (Aplicacion) getApplication();
        recyclerView=(RecyclerView) findViewById(R.id.recId);
        recyclerView.setHasFixedSize(true); // Mejorar el rendimiento
        miAdaptador = app.getAdaptador();
        recyclerView.setAdapter(miAdaptador);

        fbtnAgregar=(FloatingActionButton) findViewById(R.id.agregarAlumno);
        fbtnSalir=(FloatingActionButton) findViewById(R.id.salirApp);

        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        fbtnAgregar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                alumno=null;
                Intent intent = new Intent(MainActivity.this, AlumnoAlta.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("alumno",alumno);
                bundle.putInt("posicion",posicion);
                intent.putExtras(bundle);

                startActivityForResult(intent,0);
            }
        });

        fbtnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder confirmar = new AlertDialog.Builder(MainActivity.this);
                confirmar.setTitle("Alumnos");
                confirmar.setMessage("¿Desea salir de la app?");
                confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //No hace nada
                    }
                });
                confirmar.show();
            }
        });

        miAdaptador.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                posicion=recyclerView.getChildAdapterPosition(v);
                alumno=app.getAlumnos().get(posicion);

                Intent intent = new Intent(MainActivity.this, AlumnoAlta.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("alumno",alumno);
                bundle.putInt("posicion",posicion);
                intent.putExtras(bundle);

                startActivityForResult(intent,1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode, resultCode, intent);
        recyclerView.getAdapter().notifyDataSetChanged();

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.searchview, menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        searchView = (SearchView) menuItem.getActionView();
        setupSearchView();

        return true;
    }

    public void setupSearchView() {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                miAdaptador.filtrar(s);
                return true;
            }
        });
    }
}